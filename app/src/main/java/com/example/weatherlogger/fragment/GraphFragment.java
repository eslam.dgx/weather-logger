package com.example.weatherlogger.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.weatherlogger.R;
import com.example.weatherlogger.cash.room.viewmodel.RoomWeatherViewModel;
import com.example.weatherlogger.databinding.FragmentGraphBinding;
import com.example.weatherlogger.utils.Operation;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class GraphFragment extends Fragment {

    private FragmentGraphBinding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.fragment_graph, null, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initGraph();
        loadCashedDate();

    }

    private void initGraph() {
        binding.chart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        binding.chart.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        binding.chart.setPinchZoom(false);

        binding.chart.setDrawBarShadow(false);
        binding.chart.setDrawGridBackground(false);

        XAxis xAxis = binding.chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);

        binding.chart.getAxisLeft().setDrawGridLines(false);


        // add a nice and smooth animation
        binding.chart.animateY(1500);

        binding.chart.getLegend().setEnabled(false);
    }

    private void loadCashedDate() {

        RoomWeatherViewModel roomWeatherViewModel = ViewModelProviders.of(this).get(RoomWeatherViewModel.class);
        roomWeatherViewModel.getWeatherLiveData().observe(GraphFragment.this, weatherModels -> {

            int seekBarX = weatherModels.size();

            ArrayList<BarEntry> values = new ArrayList<>();

            for (int i = 0; i < seekBarX; i++) {
                values.add(new BarEntry(Float.parseFloat(Operation.getFormattedDay(Long.parseLong(weatherModels.get(i).getDate()))), Float.parseFloat(weatherModels.get(i).getDayTemperature()))); // tempratues
            }

            BarDataSet set1;

            set1 = new BarDataSet(values, "The year 2017");
            set1.setColors(ColorTemplate.VORDIPLOM_COLORS);


            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            binding.chart.setData(data);

            binding.chart.invalidate();

        });
    }


}
