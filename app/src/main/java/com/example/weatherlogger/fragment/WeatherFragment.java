package com.example.weatherlogger.fragment;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.weatherlogger.widget.MyAppWidget;
import com.example.weatherlogger.R;
import com.example.weatherlogger.adapter.WeatherAdapter;
import com.example.weatherlogger.cash.UtilsPreference;
import com.example.weatherlogger.cash.room.model.RoomWeatherModel;
import com.example.weatherlogger.cash.room.viewmodel.RoomWeatherViewModel;
import com.example.weatherlogger.databinding.FragmentWeatherBinding;
import com.example.weatherlogger.listener.OnWeatherItemListener;
import com.example.weatherlogger.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class WeatherFragment extends Fragment implements OnWeatherItemListener {


    private FragmentWeatherBinding binding;
    private boolean onePanel;

    public WeatherFragment(boolean onePanel) {
        this.onePanel = onePanel;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.fragment_weather, null, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadingDate();
        loadCashedDate();

    }

    private void loadCashedDate() {

        RoomWeatherViewModel roomWeatherViewModel = ViewModelProviders.of(this).get(RoomWeatherViewModel.class);
        roomWeatherViewModel.getWeatherLiveData().observe(WeatherFragment.this, weatherModels -> {

            if (weatherModels == null || weatherModels.size() == 0) {
                binding.layoutNoDate.getRoot().setVisibility(View.VISIBLE);
                if (WeatherFragment.this.getContext() == null)
                    return;

                UtilsPreference.getInstance(WeatherFragment.this.getContext()).savePreference(Constants.WIDGET_TEMP, null);
                Intent intent = new Intent(WeatherFragment.this.getContext(), MyAppWidget.class);
                intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(WeatherFragment.this.getContext());
                int[] ids = appWidgetManager.getAppWidgetIds(new ComponentName(WeatherFragment.this.getContext(), MyAppWidget.class));
                intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
                WeatherFragment.this.getContext().sendBroadcast(intent);


                ComponentName thisWidget = new ComponentName(WeatherFragment.this.getContext(), MyAppWidget.class);
                int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
                appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.example_widget_button);

                return;
            }

            binding.layoutNoDate.getRoot().setVisibility(View.GONE);
            WeatherAdapter offersAdapter = new WeatherAdapter(WeatherFragment.this.getContext(), weatherModels, WeatherFragment.this, false);
            binding.rvWeather.setLayoutManager(new LinearLayoutManager(getContext()));
            binding.rvWeather.setAdapter(offersAdapter);

        });
    }


    private void loadingDate() {
        List<RoomWeatherModel> list = new ArrayList<>();
        RoomWeatherModel model = new RoomWeatherModel();
        list.add(model);
        list.add(model);
        list.add(model);
        list.add(model);
        list.add(model);
        WeatherAdapter offersAdapter = new WeatherAdapter(WeatherFragment.this.getContext(), list, null, true);
        binding.rvWeather.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rvWeather.setItemAnimator(new DefaultItemAnimator());
        binding.rvWeather.setAdapter(offersAdapter);
    }

    @Override
    public void onWeatherItemClicked(RoomWeatherModel weatherModel) {

        if (getActivity() == null)
            return;

        if (onePanel) {

            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.exit_to_right, R.anim.exit_to_right)
                    .add(R.id.fullContent, new DetailsFragment(weatherModel))
                    .addToBackStack("DetailsFragment")
                    .commit();
        } else {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.exit_to_right, R.anim.exit_to_right)
                    .add(R.id.detailsContent, new DetailsFragment(weatherModel))
                    .addToBackStack("DetailsFragment")
                    .commit();
        }

    }

}
