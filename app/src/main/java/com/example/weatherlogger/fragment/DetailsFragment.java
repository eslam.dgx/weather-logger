package com.example.weatherlogger.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.example.weatherlogger.R;
import com.example.weatherlogger.cash.room.model.RoomWeatherModel;
import com.example.weatherlogger.databinding.FragmentDetailsBinding;
import com.example.weatherlogger.utils.Operation;

public class DetailsFragment extends Fragment {

    private FragmentDetailsBinding binding;
    private RoomWeatherModel weatherModel;

    public DetailsFragment() {
    }

    DetailsFragment(RoomWeatherModel weatherModel) {
        this.weatherModel = weatherModel;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.fragment_details, null, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindData();
    }

    private void bindData() {
        binding.setWeather(weatherModel);
        binding.imgStatus.setImageResource(Operation.getArtResourceForWeatherCondition(weatherModel.getWeatherId()));
        binding.tvLabel.setText(Operation.getFormattedMonthDay(weatherModel.getDate()));
        binding.tvDate.setText(Operation.getFormattedMonthDay(Long.decode(weatherModel.getDate())));
    }
}
