package com.example.weatherlogger.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.example.weatherlogger.R;
import com.example.weatherlogger.api.model.CityModel;
import com.example.weatherlogger.cash.UtilsPreference;
import com.example.weatherlogger.cash.room.WeatherRepository;
import com.example.weatherlogger.databinding.FragmentSettingsBinding;
import com.example.weatherlogger.sync.MyServiceSyncAdapter;

public class SettingsFragment extends Fragment implements View.OnClickListener {

    private FragmentSettingsBinding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.fragment_settings, null, false);
        binding.btnConfrim.setOnClickListener(this);
        binding.imgClear.setOnClickListener(this);
        CityModel cityModel = UtilsPreference.getInstance(SettingsFragment.this.getContext()).getCountry();
        binding.etName.setText(cityModel.getName());
        return binding.getRoot();
    }


    @Override
    public void onClick(View v) {

        if (v == null)
            return;

        switch (v.getId()) {
            case R.id.imgClear:
                binding.etName.setText(null);
                break;
            case R.id.btnConfrim:

                if (binding.etName.getText() == null || binding.etName.getText().toString().trim().isEmpty()) {
                    Toast.makeText(SettingsFragment.this.getContext(), getResources().getString(R.string.enter_city_name), Toast.LENGTH_SHORT).show();
                } else {
                    CityModel cityModel = new CityModel();
                    cityModel.setName(binding.etName.getText().toString());
                    UtilsPreference.getInstance(SettingsFragment.this.getContext()).saveCity(cityModel);
                    WeatherRepository repository = new WeatherRepository(SettingsFragment.this.getContext());
                    repository.deleteWeather();
                    MyServiceSyncAdapter.initializeSyncAdapter(SettingsFragment.this.getContext());
                    MyServiceSyncAdapter.syncImmediately(SettingsFragment.this.getContext());
                    if (getActivity() != null) {
                        InputMethodManager keyboard = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                        assert keyboard != null;
                        keyboard.hideSoftInputFromWindow(getActivity().getWindow().getDecorView().getRootView().getWindowToken(), 0);
                        getActivity().getSupportFragmentManager().popBackStack();
                    }
                }
                break;
        }


    }
}
