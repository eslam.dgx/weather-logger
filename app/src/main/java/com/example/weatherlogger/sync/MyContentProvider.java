package com.example.weatherlogger.sync;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import com.example.weatherlogger.cash.room.RoomDateBase;

import org.jetbrains.annotations.NotNull;


public class MyContentProvider extends ContentProvider {



    @Override
    public boolean onCreate() {
        return true;
    }

    @Override
    public String getType(@NotNull Uri uri) {
        return null;
    }

    @Override
    public Cursor query(@NotNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        if (getContext() != null){
            final Cursor cursor = RoomDateBase.getInstance(getContext()).weatherDao().getItemsWithCursor();
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
            return cursor;
        }

        throw new IllegalArgumentException("Failed to query row for uri " + uri);

    }

    @Override
    public Uri insert(@NotNull Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(@NotNull Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NotNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int bulkInsert(@NotNull Uri uri, @NotNull ContentValues[] values) {
        return 0;
    }

}
