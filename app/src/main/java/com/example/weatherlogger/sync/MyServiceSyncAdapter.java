package com.example.weatherlogger.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.appwidget.AppWidgetManager;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ComponentName;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SyncRequest;
import android.content.SyncResult;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.weatherlogger.widget.MyAppWidget;
import com.example.weatherlogger.R;
import com.example.weatherlogger.api.ApiClient;
import com.example.weatherlogger.api.ApiInterface;
import com.example.weatherlogger.api.model.CityModel;
import com.example.weatherlogger.api.model.WeatherModel;
import com.example.weatherlogger.api.model.WeatherModelAPI;
import com.example.weatherlogger.cash.UtilsPreference;
import com.example.weatherlogger.cash.room.WeatherRepository;
import com.example.weatherlogger.cash.room.model.RoomWeatherModel;
import com.example.weatherlogger.utils.Constants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyServiceSyncAdapter extends AbstractThreadedSyncAdapter {

    private static final int SYNC_INTERVAL = 1;       // 60 seconds * 65 = 1 hours 5 minutes, the minimum internal is 1 hour
    private static final int SYNC_FLEXTIME = SYNC_INTERVAL / 3;
    private static final String TAG = "MyServiceSyncAdapter";
    private Context context;

    MyServiceSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        this.context = context;
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        Log.i("MyServiceSyncAdapter", "onPerformSync");
        loadAndSaveData();




    }

    private void loadAndSaveData() {
        Log.d(TAG, "loadAndSaveData: ");
        CityModel cityModel = UtilsPreference.getInstance(context).getCountry();
        ApiInterface mApiService = ApiClient.getInterfaceService();
        Call<WeatherModelAPI> call = mApiService.getWeatherForecast(cityModel.getName(), 10, Constants.UNITS, Constants.APP_ID);
        call.enqueue(new Callback<WeatherModelAPI>() {
            @Override
            public void onResponse(@NonNull Call<WeatherModelAPI> call, @NonNull Response<WeatherModelAPI> response) {

                WeatherModelAPI body = response.body();
                Log.d(TAG, "onResponse: ");

                if (body == null) {
                    Log.d(TAG, "onResponse: null body");
                    return;
                }
                if (body.getCod() == 200 && body.getList() != null && body.getList().size() > 0) {
                    WeatherModel first = body.getList().get(0);
                    UtilsPreference.getInstance(context).savePreference(Constants.WIDGET_TEMP , first.getTemp().getDay());
                    Log.d("MyAppWidget", "onPerformSync: ");
                    String temp = UtilsPreference.getInstance(context).getPreference(Constants.WIDGET_TEMP, null);
                    Log.d("MyAppWidget", "onPerformSync: temp "+temp);
                    Intent intent = new Intent(context, MyAppWidget.class);
                    intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                    AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
                    int[] ids = appWidgetManager.getAppWidgetIds(new ComponentName(context, MyAppWidget.class));
                    intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
                    context.sendBroadcast(intent);
                    addUpdateDeleteLocalData(body.getList());
                }
            }

            @Override
            public void onFailure(@NonNull Call<WeatherModelAPI> call, @NonNull Throwable t) {

                Log.d(TAG, "onFailure: " + t.getMessage());

            }
        });
    }


    private void addUpdateDeleteLocalData(List<WeatherModel> list) {

        WeatherRepository repository = new WeatherRepository(context);
        repository.deleteWeather();
        int days = 1;
        for (WeatherModel weatherModel : list) {
            RoomWeatherModel roomWeatherModel = new RoomWeatherModel();

            roomWeatherModel.setDescription(weatherModel.getWeather().getMain());
            roomWeatherModel.setDayTemperature(weatherModel.getTemp().getDay());
            roomWeatherModel.setNightTemperature(weatherModel.getTemp().getNight());
            roomWeatherModel.setWeatherId(weatherModel.getWeather().getId());
            roomWeatherModel.setHumidity(weatherModel.getHumidity());
            roomWeatherModel.setPressure(weatherModel.getPressure());
            roomWeatherModel.setWind(weatherModel.getSpeed());

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
            String date = df.format(Calendar.getInstance().getTime());
            roomWeatherModel.setLogDate(date);

            long daydate = (Calendar.getInstance().getTimeInMillis() + (days * Constants.DAY_LONG));
            roomWeatherModel.setDate(String.valueOf(daydate));

            roomWeatherModel.setId(weatherModel.getDt());

            repository.insertWeather(roomWeatherModel);
            days = days + 1;
        }
    }

    /**
     * Helper method to schedule the sync adapter periodic execution
     */
    private static void configurePeriodicSync(Context context) {
        Account account = getSyncAccount(context);
        String authority = context.getString(R.string.content_authority);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // we can enable inexact timers in our periodic sync
            SyncRequest request = new SyncRequest.Builder()
                    .syncPeriodic(MyServiceSyncAdapter.SYNC_INTERVAL, MyServiceSyncAdapter.SYNC_FLEXTIME)
                    .setSyncAdapter(account, authority)
                    .setExtras(new Bundle()).build();
            ContentResolver.requestSync(request);
        } else {
            ContentResolver.addPeriodicSync(account, authority, new Bundle(), MyServiceSyncAdapter.SYNC_INTERVAL);
        }
    }

    /**
     * Helper method to have the sync adapter sync immediately
     *
     * @param context The context used to access the account service
     */

    public static void syncImmediately(Context context) {
        Log.i("MyServiceSyncAdapter", "syncImmediately");
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        ContentResolver.requestSync(getSyncAccount(context), context.getString(R.string.content_authority), bundle);
        if (ContentResolver.isSyncActive(getSyncAccount(context), context.getString(R.string.content_authority))) {
            Log.d(TAG, "syncImmediately: active");
        } else {
            Log.d(TAG, "syncImmediately: not active");
        }
        if (ContentResolver.isSyncPending(getSyncAccount(context), context.getString(R.string.content_authority))) {
            Log.d(TAG, "syncImmediately: Pending");
        } else {
            Log.d(TAG, "syncImmediately: not Pending");
        }
    }

    /**
     * Helper method to get the fake account to be used with SyncAdapter, or make a new one
     * if the fake account doesn't exist yet.  If we make a new account, we call the
     * onAccountCreated method so we can initialize things.
     *
     * @param context The context used to access the account service
     * @return a fake account.
     */
    private static Account getSyncAccount(Context context) {
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE); // Get an instance of the Android account manager
        Account newAccount = new Account(context.getString(R.string.app_name), context.getString(R.string.sync_account_type)); // Create the account type and default account

        // If the password doesn't exist, the account doesn't exist
        assert accountManager != null;
        if (accountManager.getPassword(newAccount) == null) {
            if (!accountManager.addAccountExplicitly(newAccount, "", null)) {
                Log.e("MyServiceSyncAdapter", "getSyncAccount Failed to create new account.");
                return null;
            }
            onAccountCreated(newAccount, context);
        }
        return newAccount;
    }

    private static void onAccountCreated(Account newAccount, Context context) {
        Log.i("MyServiceSyncAdapter", "onAccountCreated");
        MyServiceSyncAdapter.configurePeriodicSync(context);
        ContentResolver.setSyncAutomatically(newAccount, context.getString(R.string.content_authority), true);
        syncImmediately(context);
    }

    public static void initializeSyncAdapter(Context context) {
        Log.d("MyServiceSyncAdapter", "initializeSyncAdapter");
        getSyncAccount(context);
    }
}