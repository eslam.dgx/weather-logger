package com.example.weatherlogger.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weatherlogger.api.model.CityModel;
import com.example.weatherlogger.cash.UtilsPreference;
import com.example.weatherlogger.listener.OnWeatherItemListener;
import com.example.weatherlogger.R;
import com.example.weatherlogger.cash.room.model.RoomWeatherModel;
import com.example.weatherlogger.databinding.RvWeatherDaysItemBinding;
import com.example.weatherlogger.databinding.RvWeatherTodayItemBinding;
import com.example.weatherlogger.fragment.WeatherFragment;
import com.example.weatherlogger.utils.Operation;

import java.util.List;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.ViewHolder> {

    private static final int TODAY = 10;
    private static final int OTHER_DAYS = 20;
    private Context context;
    private List<RoomWeatherModel> weatherModels;
    private OnWeatherItemListener listener;
    private boolean isLoading;


    public WeatherAdapter(Context context, List<RoomWeatherModel> weatherModels, WeatherFragment listener, boolean isLoading) {
        this.context = context;
        this.weatherModels = weatherModels;
        this.listener = listener;
        this.isLoading = isLoading;
    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return TODAY;
        else
            return OTHER_DAYS;
    }

    @NonNull
    @Override
    public WeatherAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TODAY) {
            RvWeatherTodayItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.rv_weather_today_item, parent, false);
            binding.setListener(listener);
            return new ViewHolder(binding, viewType);
        } else {
            RvWeatherDaysItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.rv_weather_days_item, parent, false);
            binding.setListener(listener);
            return new ViewHolder(binding, viewType);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull WeatherAdapter.ViewHolder holder, int position) {
        if (position != RecyclerView.NO_POSITION)
            holder.setViewModel(weatherModels.get(position));
    }

    @Override
    public int getItemCount() {
        return weatherModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private RvWeatherTodayItemBinding bindingToday;
        private RvWeatherDaysItemBinding bindingOtherDays;
        private int viewType;

        ViewHolder(RvWeatherTodayItemBinding binding, int viewType) {
            super(binding.getRoot());
            this.viewType = viewType;
            this.bindingToday = binding;
        }

        ViewHolder(RvWeatherDaysItemBinding binding, int viewType) {
            super(binding.getRoot());
            this.viewType = viewType;
            this.bindingOtherDays = binding;
        }


        void bind() {
            if (viewType == TODAY) {
                if (bindingToday != null) {
                    bindingToday = DataBindingUtil.bind(itemView);
                }
            } else if (viewType == OTHER_DAYS) {
                if (bindingOtherDays != null) {
                    bindingOtherDays = DataBindingUtil.bind(itemView);
                }
            }
        }

        void unbind() {

            if (viewType == TODAY) {
                if (bindingToday != null) {
                    bindingToday.unbind();
                }
            } else if (viewType == OTHER_DAYS) {
                if (bindingOtherDays != null) {
                    bindingOtherDays.unbind();
                }
            }

        }

        void setViewModel(RoomWeatherModel weatherModel) {
            if (viewType == TODAY && bindingToday != null) {

                if (isLoading) {
                    bindingToday.loader.startShimmerAnimation();
                } else {
                    bindingToday.loader.stopShimmerAnimation();
                    bindingToday.setWeather(weatherModel);
                    bindingToday.imgStatus.setImageResource(Operation.getArtResourceForWeatherCondition(weatherModel.getWeatherId()));
                    bindingToday.tvDate.setText(Operation.getFormattedMonthDay( weatherModel.getDate() ));
                    CityModel cityModel = UtilsPreference.getInstance(context).getCountry();
                    bindingToday.tvCityName.setText(cityModel.getName());

                }
            } else if (viewType == OTHER_DAYS && bindingOtherDays != null) {

                if (isLoading) {
                    bindingOtherDays.loaderImgStatus.startShimmerAnimation();
                    bindingOtherDays.loaderDate.startShimmerAnimation();
                    bindingOtherDays.loaderStatus.startShimmerAnimation();
                    bindingOtherDays.loaderDegree.startShimmerAnimation();
                    bindingOtherDays.loaderSpeed.startShimmerAnimation();
                } else {
                    bindingOtherDays.loaderImgStatus.stopShimmerAnimation();
                    bindingOtherDays.loaderDate.stopShimmerAnimation();
                    bindingOtherDays.loaderStatus.stopShimmerAnimation();
                    bindingOtherDays.loaderDegree.stopShimmerAnimation();
                    bindingOtherDays.loaderSpeed.stopShimmerAnimation();

                    bindingOtherDays.imgStatus.setBackground(null);
                    bindingOtherDays.tvDate.setBackground(null);
                    bindingOtherDays.tvStatus.setBackground(null);
                    bindingOtherDays.tvDayTemp.setBackground(null);
                    bindingOtherDays.tvNightTemp.setBackground(null);

                    bindingOtherDays.setWeather(weatherModel);
                    bindingOtherDays.imgStatus.setImageResource(Operation.getArtResourceForWeatherCondition(weatherModel.getWeatherId()));
                    bindingOtherDays.tvStatus.setText(Operation.getDateFormat(weatherModel.getLogDate()));

                    bindingOtherDays.tvDate.setText(Operation.getFormattedMonthDay(  weatherModel.getDate() ));

                }


            }
        }

    }

    @Override
    public void onViewAttachedToWindow(@NonNull WeatherAdapter.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull WeatherAdapter.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

}




