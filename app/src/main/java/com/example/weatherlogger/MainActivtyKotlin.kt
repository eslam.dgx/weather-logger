package com.example.weatherlogger

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.weatherlogger.databinding.ActivityMainBinding
import com.example.weatherlogger.fragment.GraphFragment
import com.example.weatherlogger.fragment.SettingsFragment
import com.example.weatherlogger.fragment.WeatherFragment
import com.example.weatherlogger.sync.MyServiceSyncAdapter
import com.example.weatherlogger.utils.Operation

class MainActivtyKotlin : AppCompatActivity(), View.OnClickListener {


    private var onPanel = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MyServiceSyncAdapter.initializeSyncAdapter(applicationContext)
        MyServiceSyncAdapter.syncImmediately(applicationContext)
        Operation.changeStatusBarColor(this, R.color.blue)
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.layoutHeader.imgSettings.setOnClickListener(this)
        binding.layoutHeader.imgGraph.setOnClickListener(this)
        if (binding.detailsContent == null) {
            onPanel = true
            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.fullContent, WeatherFragment(onPanel))
                    .addToBackStack("WeatherFragment")
                    .commit()
        } else {
            onPanel = false
            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.mainContent, WeatherFragment(onPanel))
                    .addToBackStack("WeatherFragment")
                    .commit()


        }
    }

    override fun onClick(v: View?) {
        if (v == null) return
        when (v.id) {
            R.id.imgSettings -> if (onPanel) {
                supportFragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.exit_to_right, R.anim.exit_to_right)
                        .add(R.id.fullContent, SettingsFragment())
                        .addToBackStack("SettingsFragment")
                        .commit()
            } else {
                supportFragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.exit_to_right, R.anim.exit_to_right)
                        .add(R.id.detailsContent, SettingsFragment())
                        .addToBackStack("SettingsFragment")
                        .commit()
            }
            R.id.imgGraph -> if (onPanel) {
                supportFragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.exit_to_right, R.anim.exit_to_right)
                        .add(R.id.fullContent, GraphFragment())
                        .addToBackStack("SettingsFragment")
                        .commit()
            } else {
                supportFragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.exit_to_right, R.anim.exit_to_right)
                        .add(R.id.detailsContent, GraphFragment())
                        .addToBackStack("SettingsFragment")
                        .commit()
            }
        }
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            val fragmentsCount = supportFragmentManager.fragments.size
            if (fragmentsCount <= 1) finish()
        }
        return super.onKeyDown(keyCode, event)
    }

}
