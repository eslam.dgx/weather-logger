package com.example.weatherlogger.listener;

import com.example.weatherlogger.cash.room.model.RoomWeatherModel;

public interface OnWeatherItemListener {

    void onWeatherItemClicked(RoomWeatherModel weatherModel);
}
