package com.example.weatherlogger.api;


import com.example.weatherlogger.api.model.WeatherModelAPI;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface ApiInterface {


    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @GET("daily")
    Call<WeatherModelAPI> getWeatherForecast(@Query("lat") double lat,
                                             @Query("lon") double lon,
                                             @Query("cnt") int days,
                                             @Query("units") String units,
                                             @Query("APPID") String appID);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @GET("daily")
    Call<WeatherModelAPI> getWeatherForecast(@Query("q") String cirtName,
                                             @Query("cnt") int days,
                                             @Query("units") String units,
                                             @Query("APPID") String appID);
}