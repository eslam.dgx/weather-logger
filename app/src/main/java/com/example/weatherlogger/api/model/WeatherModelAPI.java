package com.example.weatherlogger.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeatherModelAPI {

    @SerializedName("city")
    private CityModel city;

    @SerializedName("cod")
    private int cod;

    @SerializedName("cnt")
    private String cnt;

    @SerializedName("message")
    private String message;

    @SerializedName("list")
    private List<WeatherModel> list;

    public CityModel getCity() {
        return city;
    }

    public int getCod() {
        return cod;
    }

    public String getCnt() {
        return cnt;
    }

    public String getMessage() {
        return message;
    }

    public List<WeatherModel> getList() {
        return list;
    }
}
