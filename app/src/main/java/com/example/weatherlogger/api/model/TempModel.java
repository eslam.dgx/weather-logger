package com.example.weatherlogger.api.model;

import com.google.gson.annotations.SerializedName;

public class TempModel {

    @SerializedName("day")
    private String day;

    @SerializedName("min")
    private String min;

    @SerializedName("max")
    private String max;

    @SerializedName("night")
    private String night;

    @SerializedName("eve")
    private String eve;

    @SerializedName("morn")
    private String morn;

    public String getDay() {
        return day;
    }

    public String getMin() {
        return min;
    }

    public String getMax() {
        return max;
    }

    public String getNight() {
        return night;
    }

    public String getEve() {
        return eve;
    }

    public String getMorn() {
        return morn;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public void setNight(String night) {
        this.night = night;
    }

    public void setEve(String eve) {
        this.eve = eve;
    }

    public void setMorn(String morn) {
        this.morn = morn;
    }
}
