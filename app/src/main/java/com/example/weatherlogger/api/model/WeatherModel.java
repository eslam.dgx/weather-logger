package com.example.weatherlogger.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeatherModel {

    @SerializedName("dt")
    private String dt;

    @SerializedName("sunrise")
    private String sunrise;

    @SerializedName("sunset")
    private String sunset;

    @SerializedName("pressure")
    private String pressure;

    @SerializedName("humidity")
    private String humidity;

    @SerializedName("speed")
    private String speed;

    @SerializedName("deg")
    private String deg;

    @SerializedName("clouds")
    private String clouds;

    @SerializedName("rain")
    private String rain;

    @SerializedName("temp")
    private TempModel temp;

    @SerializedName("feels_like")
    private FeelsLikeModel feels_like;

    @SerializedName("weather")
    private List<WeatherPref> weather;

    public String getDt() {
        return dt;
    }

    public String getSunrise() {
        return sunrise;
    }

    public String getSunset() {
        return sunset;
    }

    public String getPressure() {
        return pressure;
    }

    public String getHumidity() {
        return humidity;
    }

    public String getSpeed() {
        return speed;
    }

    public String getDeg() {
        return deg;
    }

    public String getClouds() {
        return clouds;
    }

    public String getRain() {
        return rain;
    }

    public TempModel getTemp() {
        return temp;
    }

    public FeelsLikeModel getFeels_like() {
        return feels_like;
    }

    public WeatherPref getWeather() {
        return weather.get(0);
    }


    public void setDt(String dt) {
        this.dt = dt;
    }

    public void setSunrise(String sunrise) {
        this.sunrise = sunrise;
    }

    public void setSunset(String sunset) {
        this.sunset = sunset;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public void setDeg(String deg) {
        this.deg = deg;
    }

    public void setClouds(String clouds) {
        this.clouds = clouds;
    }

    public void setRain(String rain) {
        this.rain = rain;
    }

    public void setTemp(TempModel temp) {
        this.temp = temp;
    }

    public void setFeels_like(FeelsLikeModel feels_like) {
        this.feels_like = feels_like;
    }

    public void setWeather(List<WeatherPref> weather) {
        this.weather = weather;
    }
}

