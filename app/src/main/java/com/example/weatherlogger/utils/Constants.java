package com.example.weatherlogger.utils;

public class Constants {

    public static final int DATA_BASE_VERSION = 13;
    public static final String DATA_BASE_NAME = "db";
    public static final String CITY_NAME = "CITY_NAME";
    public static final String CITY_ZIP_CODE = "CITY_ZIP_CODE";
    public static final long DAY_LONG = 86400000;
    public static final String DATE_FORMAT = "EEEE dd";
    public static final String DATE_FORMAT_2 = "MMMM dd";
    public static final String DATE_FORMAT_3 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String DATE_FORMAT_4 = "dd";

    public static final String UNITS = "metric";
    public static final String APP_ID = "ba9747ccb539d9615c78dd4c0d064624";
    public static final String WIDGET_TEMP = "WIDGET_TEMP" ;
}
