package com.example.weatherlogger.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build;

import androidx.core.content.ContextCompat;

import com.example.weatherlogger.R;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Operation {

    public static void changeStatusBarColor(Activity activity, int color) {
        if (activity != null && activity.getWindow() != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().setStatusBarColor(ContextCompat.getColor(activity, color));
        }
    }


    public static int getArtResourceForWeatherCondition(int weatherId) {

        if (weatherId >= 200 && weatherId <= 232) {
            return R.drawable.art_storm;
        } else if (weatherId >= 300 && weatherId <= 321) {
            return R.drawable.art_light_rain;
        } else if (weatherId >= 500 && weatherId <= 504) {
            return R.drawable.art_rain;
        } else if (weatherId == 511) {
            return R.drawable.art_snow;
        } else if (weatherId >= 520 && weatherId <= 531) {
            return R.drawable.art_rain;
        } else if (weatherId >= 600 && weatherId <= 622) {
            return R.drawable.art_snow;
        } else if (weatherId >= 701 && weatherId <= 761) {
            return R.drawable.art_fog;
        } else if (weatherId == 781) {
            return R.drawable.art_storm;
        } else if (weatherId == 800) {
            return R.drawable.art_clear;
        } else if (weatherId == 801) {
            return R.drawable.art_light_clouds;
        } else if (weatherId >= 802 && weatherId <= 804) {
            return R.drawable.art_clouds;
        }
        return -1;
    }

    public static String getFormattedMonthDay(long dateInMillis) {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat monthDayFormat = new SimpleDateFormat(Constants.DATE_FORMAT_2);
        return monthDayFormat.format(dateInMillis);
    }

    public static String getFormattedDay(long dateInMillis) {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat monthDayFormat = new SimpleDateFormat(Constants.DATE_FORMAT_4);
        return monthDayFormat.format(dateInMillis);
    }

    public static String getDateFormat(String logDate) {
        try {
            SimpleDateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT_3, Locale.US);
            Date date = df.parse(logDate);
            PrettyTime prettyTime = new PrettyTime(Locale.getDefault());

            return prettyTime.format(date);
        } catch (Exception e) {
            return logDate;
        }
    }

    public static String getFormattedMonthDay(String date) {
        long dateInMillis = Long.parseLong(date);
        long currentDate = Calendar.getInstance().getTimeInMillis();
        int diff = (int) ((dateInMillis % currentDate) / Constants.DAY_LONG);
        if (diff == 0) {
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat dayFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
            return "Today," + dayFormat.format(dateInMillis);
        } else if (diff == 1)
            return "Tomorrow";
        else {
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat dayFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
            return dayFormat.format(dateInMillis);
        }

    }
}
