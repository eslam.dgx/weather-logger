package com.example.weatherlogger.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

import com.example.weatherlogger.MainActivityJava;
import com.example.weatherlogger.R;
import com.example.weatherlogger.cash.UtilsPreference;
import com.example.weatherlogger.utils.Constants;


public class MyAppWidget extends AppWidgetProvider {

    private static final String TAG = "MyAppWidget";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            Log.d(TAG, "onUpdate: ");
            Intent intent = new Intent(context, MainActivityJava.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.my_app_widget);
            String temp = UtilsPreference.getInstance(context).getPreference(Constants.WIDGET_TEMP, null);
            Log.d(TAG, "onUpdate: temp "+temp);
            if (temp == null)
                views.setTextViewText(R.id.example_widget_button, context.getString(R.string.no_date));
            else
                views.setTextViewText(R.id.example_widget_button, temp);
            views.setOnClickPendingIntent(R.id.example_widget_button, pendingIntent);
            appWidgetManager.updateAppWidget(appWidgetId, views);
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId, R.id.example_widget_button);
        }
    }

}