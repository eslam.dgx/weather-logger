package com.example.weatherlogger;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.weatherlogger.databinding.ActivityMainBinding;
import com.example.weatherlogger.fragment.GraphFragment;
import com.example.weatherlogger.fragment.SettingsFragment;
import com.example.weatherlogger.fragment.WeatherFragment;
import com.example.weatherlogger.sync.MyServiceSyncAdapter;
import com.example.weatherlogger.utils.Operation;

public class MainActivityJava extends AppCompatActivity implements View.OnClickListener {

    private boolean onPanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyServiceSyncAdapter.initializeSyncAdapter(getApplicationContext());
        MyServiceSyncAdapter.syncImmediately(getApplicationContext());
        Operation.changeStatusBarColor(this, R.color.blue);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.layoutHeader.imgSettings.setOnClickListener(this);
        binding.layoutHeader.imgGraph.setOnClickListener(this);
        if (binding.detailsContent == null) {
            onPanel = true;
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fullContent, new WeatherFragment(onPanel))
                    .addToBackStack("WeatherFragment")
                    .commit();

        } else {
            onPanel = false;
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.mainContent, new WeatherFragment(onPanel))
                    .addToBackStack("WeatherFragment")
                    .commit();
        }

    }

    @Override
    public void onClick(View v) {

        if (v == null)
            return;
        switch (v.getId()) {
            case R.id.imgSettings:
                if (onPanel) {
                    getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.exit_to_right, R.anim.exit_to_right)
                            .add(R.id.fullContent, new SettingsFragment())
                            .addToBackStack("SettingsFragment")
                            .commit();
                } else {
                    getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.exit_to_right, R.anim.exit_to_right)
                            .add(R.id.detailsContent, new SettingsFragment())
                            .addToBackStack("SettingsFragment")
                            .commit();
                }
                break;
            case R.id.imgGraph:
                if (onPanel) {
                    getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.exit_to_right, R.anim.exit_to_right)
                            .add(R.id.fullContent, new GraphFragment())
                            .addToBackStack("SettingsFragment")
                            .commit();
                } else {
                    getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.exit_to_right, R.anim.exit_to_right)
                            .add(R.id.detailsContent, new GraphFragment())
                            .addToBackStack("SettingsFragment")
                            .commit();
                }
                break;
        }

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            int fragmentsCount = getSupportFragmentManager().getFragments().size();
            if (fragmentsCount <= 1)
                finish();
        }
        return super.onKeyDown(keyCode, event);
    }

}
