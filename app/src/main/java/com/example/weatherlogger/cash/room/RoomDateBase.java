package com.example.weatherlogger.cash.room;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.weatherlogger.cash.room.dao.RoomWeatherDao;
import com.example.weatherlogger.cash.room.model.RoomWeatherModel;

import static com.example.weatherlogger.utils.Constants.DATA_BASE_NAME;
import static com.example.weatherlogger.utils.Constants.DATA_BASE_VERSION;


@Database(entities = {RoomWeatherModel.class}, version = DATA_BASE_VERSION, exportSchema = false)
public abstract class RoomDateBase extends RoomDatabase {





    private static RoomDateBase instance;

    public abstract RoomWeatherDao weatherDao();


    public static RoomDateBase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    RoomDateBase.class, DATA_BASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }


}
