package com.example.weatherlogger.cash.room.dao;

import android.database.Cursor;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.weatherlogger.cash.room.model.RoomWeatherModel;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface RoomWeatherDao {

    @Insert(onConflict = REPLACE)
    void insert(RoomWeatherModel model);

    @Update()
    void update(RoomWeatherModel model);

    @Query("DELETE FROM RoomWeatherModel")
    void deleteWeather();

    @Query("SELECT * FROM RoomWeatherModel")
    LiveData<List<RoomWeatherModel>> getWeatherLiveData();


    @Query("SELECT * FROM RoomWeatherModel")
    Cursor getItemsWithCursor();


}
