package com.example.weatherlogger.cash.room;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.weatherlogger.cash.room.dao.RoomWeatherDao;
import com.example.weatherlogger.cash.room.model.RoomWeatherModel;

import java.util.List;

public class WeatherRepository {

    private RoomWeatherDao weatherDao;
    private LiveData<List<RoomWeatherModel>> weatherLiveData;


    public WeatherRepository(Context application) {
        RoomDateBase database = RoomDateBase.getInstance(application);
        weatherDao = database.weatherDao();
        weatherLiveData = weatherDao.getWeatherLiveData();

    }


    public LiveData<List<RoomWeatherModel>> getWeatherLiveData() {
        return weatherLiveData;
    }

    public void insertWeather(RoomWeatherModel model) {
        new InsetWeatherTask(weatherDao, model).execute();
    }

    public void deleteWeather() {
        new DeleteWeatherTask(weatherDao).execute();
    }

    public void updateWeather(RoomWeatherModel RoomWeatherModel) {
        new UpdateWeatherTask(weatherDao, RoomWeatherModel).execute();
    }


    private static class DeleteWeatherTask extends AsyncTask<Void, Void, Void> {
        private RoomWeatherDao dao;


        DeleteWeatherTask(RoomWeatherDao dao) {
            this.dao = dao;

        }


        @Override
        protected Void doInBackground(Void... PromoCodeModels) {
            dao.deleteWeather();
            return null;
        }
    }

    private static class InsetWeatherTask extends AsyncTask<Void, Void, Void> {
        private RoomWeatherDao dao;
        private RoomWeatherModel model;

        InsetWeatherTask(RoomWeatherDao dao, RoomWeatherModel model) {
            this.dao = dao;
            this.model = model;
        }


        @Override
        protected Void doInBackground(Void... PromoCodeModels) {
            dao.insert(model);
            return null;
        }
    }

    private static class UpdateWeatherTask extends AsyncTask<Void, Void, Void> {

        private RoomWeatherDao dao;
        private RoomWeatherModel RoomWeatherModel;

        UpdateWeatherTask(RoomWeatherDao currentLocationDao, RoomWeatherModel RoomWeatherModel) {
            this.dao = currentLocationDao;
            this.RoomWeatherModel = RoomWeatherModel;
        }

        @Override
        protected Void doInBackground(Void... PromoCodeModels) {
            dao.update(RoomWeatherModel);
            return null;
        }


    }
}
