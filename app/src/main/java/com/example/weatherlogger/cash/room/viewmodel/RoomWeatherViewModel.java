package com.example.weatherlogger.cash.room.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.weatherlogger.cash.room.WeatherRepository;
import com.example.weatherlogger.cash.room.model.RoomWeatherModel;

import java.util.List;

public class RoomWeatherViewModel extends AndroidViewModel {

    private LiveData<List<RoomWeatherModel>> weatherLiveData;


    public RoomWeatherViewModel(@NonNull Application application) {
        super(application);
        WeatherRepository repository = new WeatherRepository(application);
        weatherLiveData = repository.getWeatherLiveData();
    }

    public LiveData<List<RoomWeatherModel>> getWeatherLiveData() {
        return weatherLiveData;
    }
}
