package com.example.weatherlogger.cash;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import com.example.weatherlogger.R;
import com.example.weatherlogger.api.model.CityModel;
import com.example.weatherlogger.utils.Constants;

import static android.content.Context.MODE_PRIVATE;

public class UtilsPreference {

    private SharedPreferences sharedPreferences;
    private Context context;

    @NonNull
    public static UtilsPreference getInstance(Context context) {
        return new UtilsPreference(context);
    }

    private UtilsPreference(Context context) {
        if (context != null) {
            sharedPreferences = context.getSharedPreferences(context.getApplicationContext().getPackageName(), MODE_PRIVATE);
            this.context = context;
        }
    }


    public void savePreference(String key, String value) {
        if (sharedPreferences != null)
            sharedPreferences.edit().putString(key, value).apply();
    }

    public void savePreference(String key, int value) {
        if (sharedPreferences != null)
            sharedPreferences.edit().putInt(key, value).apply();
    }

    public void savePreference(String key, boolean value) {
        if (sharedPreferences != null)
            sharedPreferences.edit().putBoolean(key, value).apply();
    }

    // get Data ..
    public String getPreference(String key, String defaultValue) {
        if (sharedPreferences != null) {
            return sharedPreferences.getString(key, defaultValue);
        } else
            return null;
    }

    public int getPreference(String key, int defaultValue) {
        return sharedPreferences.getInt(key, defaultValue);
    }

    public int getVat(String key, String defaultValue) {
        return Integer.parseInt(sharedPreferences.getString(key, defaultValue));
    }

    public boolean getPreference(String key, boolean defaultValue) {
        return sharedPreferences.getBoolean(key, defaultValue);
    }


    public void saveCity(CityModel cityModel) {
        savePreference(Constants.CITY_NAME, cityModel.getName());
        savePreference(Constants.CITY_ZIP_CODE, cityModel.getZipCode());

    }

    public CityModel getCountry() {
        CityModel city = new CityModel();
        city.setName(getPreference(Constants.CITY_NAME, context.getString(R.string.riga)));
        city.setZipCode(getPreference(Constants.CITY_ZIP_CODE, null));
        return city;
    }


}
